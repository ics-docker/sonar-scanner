FROM europeanspallationsource/oracle-jdk:8

ENV SONAR_SCANNER_VERSION 3.0.3.778

RUN curl -LO https://sonarsource.bintray.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-${SONAR_SCANNER_VERSION}-linux.zip \
  && unzip sonar-scanner-cli-${SONAR_SCANNER_VERSION}-linux.zip -d /opt \
  && rm -f sonar-scanner-cli-${SONAR_SCANNER_VERSION}-linux.zip

# Update global settings to point to our SonarQube server
COPY sonar-scanner.properties /opt/sonar-scanner-${SONAR_SCANNER_VERSION}-linux/conf/

ENV PATH "/opt/sonar-scanner-${SONAR_SCANNER_VERSION}-linux/bin:${PATH}"
