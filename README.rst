sonar-scanner
=============

**DEPRECATED** - Use the official image https://hub.docker.com/r/sonarsource/sonar-scanner-cli

Docker_ image based on europeanspallationsource/oracle-jdk:8 to run sonar-scanner.

See https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner

Building
--------

This image is built automatically by gitlab-ci.

.. _Docker: https://www.docker.com
